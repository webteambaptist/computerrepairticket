﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using OneSignAuthInterop;
using System.IO;
using System.Reflection;

namespace ComputerRepairTicket
{
    class Imprivata
    {
        //private OneSignEventProvider oneSign = new OneSignEventProvider();

        public void InitImprivata()
        {

            try
            {

                //OneSignAuthSrv oneSignAuthSrv = new OneSignAuthSrv();
                //OneSignAuth oneSignAuth = new OneSignAuth();
                //IOneSignAuth12 oneSignAuth12 = (IOneSignAuth12)oneSignAuth;
                
                //IOneSignAuth13 oneSignAuth13 = (IOneSignAuth13)oneSignAuth;

            }
            catch (COMException ex)
            {
                
            }
        }
        
        public IComUserIdentity getUserIdentity()
        {
            try
            {
                List<Action> actions = new List<Action>();
                Assembly myDll = Assembly.LoadFrom(AppDomain.CurrentDomain.BaseDirectory + @"OneSignAuthInterop.dll");
                Type[] types = myDll.GetExportedTypes();
                IComUserIdentity userIdentity = null;
                for (int i = 0; i < types.Length; i++)
                {
                    Type type = types[i];
                    if (type.GetInterface("OneSignAuthInterop.OneSignAuth") != null && type != null)
                    {
                        IOneSignAuth oneSignAuth = myDll.CreateInstance(type.FullName) as IOneSignAuth;

                        userIdentity = (IComUserIdentity)oneSignAuth.CurrentOneSignUser;
                    }
                }

                Console.WriteLine(userIdentity.DisplayName);
                return userIdentity;
            }
            catch
            {
                return null;
            }
        }
        //public IComUserIdentity getWindowsIdentity()
        //{
        //    OneSignAuth oneSignAuth = new OneSignAuth();
        //    IComWindowsIdentity windowsIdentity = (IComWindowsIdentity)oneSignAuth.CurrentWindowsUser;
        //    Console.WriteLine(windowsIdentity.DisplayName);
        //    return windowsIdentity;
        //}

    }
}
