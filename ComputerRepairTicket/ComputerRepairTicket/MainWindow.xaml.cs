﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using ComputerRepairTicket.Models;
using System.Text.RegularExpressions;
using System.DirectoryServices.AccountManagement;

namespace ComputerRepairTicket
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string email;
        public MainWindow()
        {
            try
            {
                Imprivata Imprivata = new Imprivata();
                Imprivata.InitImprivata();
                var userIdentity = Imprivata.getUserIdentity();
                InitializeComponent();
                if (userIdentity == null)
                {
                    userIdentityNameLabel.Content = UserPrincipal.Current.DisplayName + "!";
                    emailTextBox.Content = getEmail(UserPrincipal.Current.SamAccountName);
                    computerSerialTextBox.Text = getSerial();
                    phoneNumberTextBox.IsEnabled = true;
                    yesRadioButton.IsChecked = true; //set as default 
                }
                else
                {
                    userIdentityNameLabel.Content = userIdentity.DisplayName + "!";
                    emailTextBox.Content = getEmail(userIdentity.Username);
                    computerSerialTextBox.Text = getSerial();
                    yesRadioButton.IsChecked = true; //set as default 
                }
            }
            catch (Exception ex)
            {

                //emailTextBox.Content = getEmail(UserPrincipal.Current.SamAccountName);

                // System.Windows.MessageBox.Show("Error: " + ex.Message);
                System.Windows.MessageBox.Show("Error! Please Try again.");
                System.Windows.Application.Current.Shutdown();
            }
        }

        ///// <summary>
        ///// Checks to see if Impravada is installed for SSO
        ///// (Depricated)
        ///// </summary>
        ///// <returns>true/false</returns>
        //private bool IsSSO()
        //{
        //    bool result = false;
        //    string path = @"C:\Program Files (x86)\Imprivata\OneSign Agent\SSOManHost.exe";

        //    if (File.Exists(path))
        //    {
        //        result = true;
        //    }

        //    return result;
        //}


        private string getSerial()
        {
            try
            {
                return SystemHelper.GetClientName();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private void exitButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void submitButton_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = validateEntry();
            if (isValid)
            {
                ConfirmationPage confirmationPage = gatherData();
                _MainWindow.NavigationService.Navigate(confirmationPage);
            }
            else
            {
                System.Windows.MessageBox.Show("All Fields are NOT valid! Please Fill out the form completely and try again.");
            }
        }

        private bool validateEntry()
        {
            try
            {
                bool isValid = true;

                if (!Regex.Match(emailTextBox.Content.ToString(), @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase).Success && !email.Contains(" "))
                {
                    isValid = false;
                    validationErrorEmail.Visibility = Visibility.Visible;
                    validationErrorEmailLabel.Visibility = Visibility.Visible;
                    validationErrorEmailLocator.Visibility = Visibility.Visible;
                }
                else
                {

                    validationErrorEmail.Visibility = Visibility.Hidden;
                    validationErrorEmailLabel.Visibility = Visibility.Hidden;
                    validationErrorEmailLocator.Visibility = Visibility.Hidden;
                }

                if (yesRadioButton.IsChecked == false && noRadioButton.IsChecked == false)
                {
                    isValid = false;
                    validationErrorRadioButtons.Visibility = Visibility.Visible;
                    validationErrorRadioButtonsLabel.Visibility = Visibility.Visible;
                    validationErrorRadioButtonsLocator.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorRadioButtons.Visibility = Visibility.Hidden;
                    validationErrorRadioButtonsLabel.Visibility = Visibility.Hidden;
                    validationErrorRadioButtonsLocator.Visibility = Visibility.Hidden;
                }

                if (yesRadioButton.IsChecked == true)
                {
                    validationErrorComputerSerialNumber.Visibility = Visibility.Hidden;
                    validationErrorComputerSerialNumberLabel.Visibility = Visibility.Hidden;
                    validationErrorComputerSerialNumberLocator.Visibility = Visibility.Hidden;
                }

                if (computerSerialTextBox.IsEnabled == true)
                {
                    if (noRadioButton.IsChecked == true)
                    {

                        if (string.IsNullOrEmpty(computerSerialTextBox.Text))
                        {
                            isValid = false;
                            validationErrorComputerSerialNumber.Visibility = Visibility.Visible;
                            validationErrorComputerSerialNumberLabel.Visibility = Visibility.Visible;
                            validationErrorComputerSerialNumberLocator.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            validationErrorComputerSerialNumber.Visibility = Visibility.Hidden;
                            validationErrorComputerSerialNumberLabel.Visibility = Visibility.Hidden;
                            validationErrorComputerSerialNumberLocator.Visibility = Visibility.Hidden;
                        }

                    }

                }
                else
                {
                    computerSerialTextBox.Text = System.Environment.MachineName;
                }

                if (string.IsNullOrEmpty(textBlock.Text))
                {
                    isValid = false;
                    validationErrorTextBlock.Visibility = Visibility.Visible;
                    validationErrorTextBlockLabel.Visibility = Visibility.Visible;
                    validationErrorTextBlockLocator.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorTextBlock.Visibility = Visibility.Hidden;
                    validationErrorTextBlockLabel.Visibility = Visibility.Hidden;
                    validationErrorTextBlockLocator.Visibility = Visibility.Hidden;
                }

                if (!Regex.Match(phoneNumberTextBox.Text, @"^[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){3}$").Success)
                {
                    // phone number was incorrect
                    isValid = false;
                    validationErrorPhoneNumber.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberLabel.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberLocator.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberFormatLabel.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorPhoneNumber.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberLabel.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberLocator.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberFormatLabel.Visibility = Visibility.Hidden;
                }

                if (string.IsNullOrEmpty(campusTextBox.Text) || string.IsNullOrEmpty(buildingTextBox.Text) || string.IsNullOrEmpty(floorTextBox.Text) || string.IsNullOrEmpty(unitRoomTextBox.Text))
                {
                    if (string.IsNullOrEmpty(campusTextBox.Text))
                    {
                        isValid = false;
                        validationErrorCampus.Visibility = Visibility.Visible;
                        validationErrorLocationLabel.Visibility = Visibility.Visible;
                        validationErrorLocationLocator.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorCampus.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(floorTextBox.Text))
                    {
                        isValid = false;
                        validationErrorFloor.Visibility = Visibility.Visible;
                        validationErrorLocationLabel.Visibility = Visibility.Visible;
                        validationErrorLocationLocator.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorFloor.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(unitRoomTextBox.Text))
                    {
                        isValid = false;
                        validationErrorUnitRoom.Visibility = Visibility.Visible;
                        validationErrorLocationLabel.Visibility = Visibility.Visible;
                        validationErrorLocationLocator.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorUnitRoom.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(buildingTextBox.Text))
                    {
                        isValid = false;
                        validationErrorBuilding.Visibility = Visibility.Visible;
                        validationErrorLocationLabel.Visibility = Visibility.Visible;
                        validationErrorLocationLocator.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorBuilding.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    validationErrorCampus.Visibility = Visibility.Hidden;
                    validationErrorBuilding.Visibility = Visibility.Hidden;
                    validationErrorFloor.Visibility = Visibility.Hidden;
                    validationErrorUnitRoom.Visibility = Visibility.Hidden;
                    validationErrorLocationLabel.Visibility = Visibility.Hidden;
                    validationErrorLocationLocator.Visibility = Visibility.Hidden;
                }

                if (string.IsNullOrEmpty(alternateNameTextBox.Text))
                {
                    isValid = false;
                    validationErrorAlternateName.Visibility = Visibility.Visible;
                    validationErrorAlternateNameLabel.Visibility = Visibility.Visible;
                    validationErrorAlternateNameLocator.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorAlternateName.Visibility = Visibility.Hidden;
                    validationErrorAlternateNameLabel.Visibility = Visibility.Hidden;
                    validationErrorAlternateNameLocator.Visibility = Visibility.Hidden;
                }

                if (!Regex.Match(alternatePhoneNumberTextBox.Text, @"^[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){3}$").Success)
                {
                    // phone number was incorrect
                    isValid = false;
                    validationErrorAlternatePhoneNumber.Visibility = Visibility.Visible;
                    validationErrorAlternatePhoneNumberLabel.Visibility = Visibility.Visible;
                    validationErrorAlternatePhoneNumberLocator.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorAlternatePhoneNumber.Visibility = Visibility.Hidden;
                    validationErrorAlternatePhoneNumberLabel.Visibility = Visibility.Hidden;
                    validationErrorAlternatePhoneNumberLocator.Visibility = Visibility.Hidden;
                }

                return isValid;

            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! Please make sure there is an online connection.");
                System.Windows.Application.Current.Shutdown();
                return false;
            }
        }
        private ConfirmationPage gatherData()
        {
            bool isValid = true;

            ConfirmationPage confirmationPage = new ConfirmationPage();

            try
            {
                //now populate results on the second panel

                //Hide all labels from the MainWindow1
                MainWindow1.additionalInformationLabel.Visibility = Visibility.Hidden;
                MainWindow1.alternateNameLabel.Visibility = Visibility.Hidden;
                MainWindow1.alternatePhoneNumberLabel.Visibility = Visibility.Hidden;
                MainWindow1.campusLabel.Visibility = Visibility.Hidden;
                MainWindow1.computerSerialLabel.Visibility = Visibility.Hidden;
                MainWindow1.contactNumberLabel.Visibility = Visibility.Hidden;
                MainWindow1.buildingLabel.Visibility = Visibility.Hidden;
                MainWindow1.floorLabel.Visibility = Visibility.Hidden;
                MainWindow1.unitRoomLabel.Visibility = Visibility.Hidden;
                MainWindow1.ifPrinterLabel.Visibility = Visibility.Hidden;
                MainWindow1.immediateAssistanceLabel.Visibility = Visibility.Hidden;
                MainWindow1.instructionsLabel.Visibility = Visibility.Hidden;
                MainWindow1.instructionsLabel.Visibility = Visibility.Hidden;
                MainWindow1.isAssistanceNeededLabel.Visibility = Visibility.Hidden;
                MainWindow1.provideLocationOfComputerOrPrinterLabel.Visibility = Visibility.Hidden;
                MainWindow1.userIdentityNameLabel.Visibility = Visibility.Hidden;
                MainWindow1.welcomeLabel.Visibility = Visibility.Hidden;
                MainWindow1.emailLabel.Visibility = Visibility.Hidden;

                //Hide all TextBoxes from the MainWindow1
                MainWindow1.alternateNameTextBox.Visibility = Visibility.Hidden;
                MainWindow1.alternatePhoneNumberTextBox.Visibility = Visibility.Hidden;
                MainWindow1.campusTextBox.Visibility = Visibility.Hidden;
                MainWindow1.computerSerialTextBox.Visibility = Visibility.Hidden;
                MainWindow1.buildingTextBox.Visibility = Visibility.Hidden;
                MainWindow1.floorTextBox.Visibility = Visibility.Hidden;
                MainWindow1.unitRoomTextBox.Visibility = Visibility.Hidden;
                MainWindow1.phoneNumberTextBox.Visibility = Visibility.Hidden;
                MainWindow1.emailTextBox.Visibility = Visibility.Hidden;

                //Pass all TextBox info along to ConfirmationPage Labels
                confirmationPage.alternateNameTextBoxLabel.Content = MainWindow1.alternateNameTextBox.Text;
                confirmationPage.alternatePhoneNumberTextBoxLabel.Content = MainWindow1.alternatePhoneNumberTextBox.Text;
                confirmationPage.campusTextBoxLabel.Content = MainWindow1.campusTextBox.Text;
                confirmationPage.computerSerialTextBoxLabel.Content = MainWindow1.computerSerialTextBox.Text;
                confirmationPage.buildingTextBoxLabel.Content = MainWindow1.buildingTextBox.Text;
                confirmationPage.floorTextBoxLabel.Content = MainWindow1.floorTextBox.Text;
                confirmationPage.unitRoomTextBoxLabel.Content = MainWindow1.unitRoomTextBox.Text;
                confirmationPage.phoneNumberTextBoxLabel.Content = MainWindow1.phoneNumberTextBox.Text;


                //Pass all TextBox info along to ConfirmationPage Edit variables
                confirmationPage.alternateNameTextBoxEdit.Text = MainWindow1.alternateNameTextBox.Text;
                confirmationPage.alternatePhoneNumberTextBoxEdit.Text = MainWindow1.alternatePhoneNumberTextBox.Text;
                confirmationPage.campusTextBoxEdit.Text = MainWindow1.campusTextBox.Text;
                confirmationPage.computerSerialTextBoxEdit.Text = MainWindow1.computerSerialTextBox.Text;
                confirmationPage.buildingTextBoxEdit.Text = MainWindow1.buildingTextBox.Text;
                confirmationPage.floorTextBoxEdit.Text = MainWindow1.floorTextBox.Text;
                confirmationPage.unitRoomTextBoxEdit.Text = MainWindow1.unitRoomTextBox.Text;
                confirmationPage.phoneNumberTextBoxEdit.Text = MainWindow1.phoneNumberTextBox.Text;
                confirmationPage.emailTextBoxEdit.Content = MainWindow1.emailTextBox.Content;

                //Hide the TextBlock from the MainWindow1
                MainWindow1.textBlock.Visibility = Visibility.Hidden;

                //Hide the MainWindow border
                MainWindow1.outerBorder.Visibility = Visibility.Hidden;

                //Pass the TextBlock info along to ConfirmationPage
                confirmationPage.textBlockLabel.Text = MainWindow1.textBlock.Text;

                //Change TextBlock to read only
                confirmationPage.textBlockLabel.IsReadOnly = true;

                //Pass the TextBlock info along to ConfirmationPage Edit variable
                confirmationPage.textBlockEdit.Text = MainWindow1.textBlock.Text;

                //Hide all Radio Buttons from the MainWindow1
                MainWindow1.noRadioButton.Visibility = Visibility.Hidden;
                MainWindow1.yesRadioButton.Visibility = Visibility.Hidden;

                //Pass all Radio Button info along to ConfirmationPage
                confirmationPage.noRadioButton.IsChecked = MainWindow1.noRadioButton.IsChecked.Value;
                confirmationPage.yesRadioButton.IsChecked = MainWindow1.yesRadioButton.IsChecked.Value;

                //Hide all Buttons from the MainWindow1
                MainWindow1.submitButton.Visibility = Visibility.Hidden;
                MainWindow1.exitButton.Visibility = Visibility.Hidden;

                confirmationPage.confirmationPageDockPanel.Visibility = Visibility.Visible;

                confirmationPage.userIdentityNameLabel.Content = userIdentityNameLabel.Content;
            }
            catch (Exception ex)
            {
                ////Program.LogException(ex, false);
                isValid = false;
            }

            if (isValid)
            {
                return confirmationPage;
            }
            else
            {
                MessageBox.Show("An error occured. Please try again.");
                System.Windows.Application.Current.Shutdown();
                return null;
            }
        }

        private string getEmail(string user)
        {
            try
            {
                PeopleSearch.People ps = new PeopleSearch.People();
                ps.UseDefaultCredentials = true;
                PeopleSearch.PrincipalInfo[] plist = ps.SearchPrincipals(user, 1, PeopleSearch.SPPrincipalType.User);
                if (plist.Length > 0)
                {
                    for (int x = 0; x < plist.Length; x++)
                    {
                        if (!string.IsNullOrEmpty(plist[x].Email))
                        {
                            email = plist[x].Email;
                        }
                    }
                }
            }
            catch (Exception ee)
            {

            }

            if(string.IsNullOrWhiteSpace(email))
            {
                email = user + "@bh.local";
            }
         

            return email;
        }

        private void yesRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            
            computerSerialTextBox.Text = getSerial();
            computerSerialTextBox.IsEnabled = false;
            //computerSerialTextBox.Text = System.Environment.MachineName;
        }

        private void noRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            computerSerialTextBox.IsEnabled = true;
            computerSerialTextBox.Clear();
            computerSerialTextBox.Text = "";
            //computerSerialTextBox.Text = null;
        }
    }
}
