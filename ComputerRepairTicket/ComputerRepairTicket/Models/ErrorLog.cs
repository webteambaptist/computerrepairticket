﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace ComputerRepairTicket.Models
{
    public class ErrorLog 
    {

        #region Properties

        /// <summary>
        /// Gets/Sets the ID.
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets/Sets the Exception.
        /// </summary>
        public string Exception { get; set; }

        /// <summary>
        /// Gets/Sets the CRTicketVersion
        /// </summary>
        public string CRTicketVersion { get; set; }

        /// <summary>
        /// Gets/Sets the serial number
        /// </summary>
        public string Serial { get; set; }

        /// <summary>
        /// Gets/Sets the LSID
        /// </summary>
        public string LSID { get; set; }

        /// <summary>
        /// Gets/Sets the Current UserId
        /// </summary>
        public string CurrentUserId { get; set; }

        /// <summary>
        /// Gets/Sets whether the error is fatal
        /// </summary>
        public bool IsFatal { get; set; }

        /// <summary>
        /// Gets/Sets the ICAVersion
        /// </summary>
        public string ICAVersion { get; set; }

        /// <summary>
        /// Gets/Sets the IEVersion
        /// </summary>
        public string IEVersion { get; set; }

        /// <summary>
        /// Gets/Sets the App
        /// </summary>
        public string Application { get; set; }

        /// <summary>
        /// Gets/Sets the last update datetime
        /// </summary>
        public DateTime lup_dt { get; set; }

        /* Display Fields */

        /// <summary>
        /// Gets/Sets the WTS Printer Location.
        /// </summary>
        public string WTSLocation { get; set; }

        /* End Display Fields */

        #endregion

        public ErrorLog() { }

        #region "INSERTS"
        public static void insErrorLog(ErrorLog e)
        {
            //            string sql = @"INSERT INTO SSO_ErrorLog ([Exception],[CRTicketVersion],[Serial],[LSID],[CurrentUserId],[IsFatal],[ICAVersion],[IEVersion], [Application]) 
            //                                             VALUES (@Exception,@CRTicketVersion,@Serial,@LSID,@CurrentUserId,@IsFatal,@ICAVersion,@IEVersion, @Application); SELECT @@IDENTITY as id";

            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insErrorLog_CRTicket", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("Exception", e.Exception);
                    sqlcmd.Parameters.AddWithValue("CRTicketVersion", e.CRTicketVersion);
                    sqlcmd.Parameters.AddWithValue("Serial", e.Serial);
                    sqlcmd.Parameters.AddWithValue("LSID", e.LSID);
                    sqlcmd.Parameters.AddWithValue("CurrentUserId", e.CurrentUserId);
                    sqlcmd.Parameters.AddWithValue("IsFatal", e.IsFatal);
                    sqlcmd.Parameters.AddWithValue("ICAVersion", e.ICAVersion);
                    sqlcmd.Parameters.AddWithValue("IEVersion", e.IEVersion);
                    sqlcmd.Parameters.AddWithValue("Application", e.Application);
                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                { }//end catch
                catch (Exception ee)
                { }//end catch

            }//end sql 


        }
        #endregion

        public static void AddErrorToLog(Exception ex, string UserId, string App, bool isFatal)
        {
            try
            {
                insErrorLog(new ErrorLog
                {
                    Exception = ex.ToString()
                    ,
                    CurrentUserId = UserId.ToUpper()
                    ,
                    ICAVersion = Machine.GetIcaVersion()
                    ,
                    IEVersion = Machine.GetIEVersion()
                    ,
                    Serial = System.Environment.MachineName
                    ,
                    CRTicketVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString()
                    ,
                    IsFatal = isFatal
                    ,
                    LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper()
                    ,
                    Application = App
                });
            }
            catch { } //do nothing if network is unavailable
        }

    }
}
