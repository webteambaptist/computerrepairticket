﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ComputerRepairTicket.Models
{
    public sealed class XMLEntry
    {
        public XMLEntry() { }
        public DateTime timeStamp { get; set; }
        public string eventType { get; set; }
        public string ntID { get; set; }
        public string machine { get; set; }
        public string app { get; set; }
        public string timeElapsed { get; set; }
    }

    public sealed class XMLEventEntry
    {
        public XMLEventEntry() { }
        public DateTime timeStamp { get; set; }
        public string eventCode { get; set; }
        public string ntID { get; set; }
        public string machine { get; set; }
        public string application { get; set; }
        public string menuItemId { get; set; }
        public string LengthOfTime { get; set; }
    }

    public sealed class XMLErrorEntry
    {
        public XMLErrorEntry() { }
        public DateTime timeStamp { get; set; }
        public string exception { get; set; }
        public string CRTicketVersion { get; set; }
        public string serial { get; set; }
        public string LSID { get; set; }
        public string currentUserId { get; set; }
        public bool isFatal { get; set; }
        public string ICAVersion { get; set; }
        public string IEVersion { get; set; }
        public string application { get; set; }
        public DateTime lup_dt { get; set; }
    }
}
