﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ComputerRepairTicket.Models
{
    /// <summary>
    /// Service Form Data Object and Methods
    /// </summary>
    public class ServiceFormData 
    {
        public int? ID { get; set; }

        public string Email { get; set; }

        public string UserName { get; set; }

        public string UserID { get; set; }

        public string PCName { get; set; }

        public string LSID { get; set; }

        public string ContactPhone { get; set; }

        public string AltName { get; set; }

        public string AltPhone { get; set; }
        
        public string Campus { get; set; }

        public string Building { get; set; }

        public string Floor { get; set; }
        
        public string UnitRoom { get; set; }

        public string Message { get; set; }

        public DateTime? lup_dt { get; set; }

        public ServiceFormData()
        {

        }

        public static void SaveFormData(ServiceFormData sfd)
        {

            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.insServiceFormData", sqlConn)  //to do change to use SQL call and cache
                    {
                        CommandType = CommandType.StoredProcedure
                    };

                    sqlcmd.Parameters.AddWithValue("Email", sfd.Email);
                    sqlcmd.Parameters.AddWithValue("UserName", sfd.UserName);
                    sqlcmd.Parameters.AddWithValue("UserID", sfd.UserID);
                    sqlcmd.Parameters.AddWithValue("PCName", sfd.PCName);
                    sqlcmd.Parameters.AddWithValue("LSID", sfd.LSID);
                    sqlcmd.Parameters.AddWithValue("ContactPhone", sfd.ContactPhone);
                    sqlcmd.Parameters.AddWithValue("AltName", sfd.AltName);
                    sqlcmd.Parameters.AddWithValue("AltPhone", sfd.AltPhone);
                    sqlcmd.Parameters.AddWithValue("Campus", sfd.Campus);
                    sqlcmd.Parameters.AddWithValue("Building", sfd.Building);
                    sqlcmd.Parameters.AddWithValue("Floor", sfd.Floor);
                    sqlcmd.Parameters.AddWithValue("UnitRoom", sfd.UnitRoom);
                    sqlcmd.Parameters.AddWithValue("Message", sfd.Message);
                    sqlcmd.Parameters.AddWithValue("lup_dt", DateTime.Now);

                    sqlcmd.ExecuteNonQuery();

                }//end try
                catch (SqlException se)
                {
                    ErrorLog.AddErrorToLog(se, sfd.UserID, "ComputerRepairTicket", false);
                    // EventLog.WriteEntry("QL", se.Message);
                }//end catch
                catch (Exception ee)
                {
                    ErrorLog.AddErrorToLog(ee, sfd.UserID, "ComputerRepairTicket", false);
                    // EventLog.WriteEntry("QL", ee.Message);
                }//end catch

            }//end sql
        }
    }
}
