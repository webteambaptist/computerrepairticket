﻿// Decompiled with JetBrains decompiler
// Type: OneSignAuthInterop.IOneSignAuthPrivate
// Assembly: OneSignAuthInterop, Version=4.1.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2F72B1BC-2D8A-4D0A-8B25-CC84D1D2B1E2
// Assembly location: C:\Source\Web Team Legacy\ComputerRepairTicket\ComputerRepairTicket\bin\Debug\OneSignAuthInterop.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace OneSignAuthInterop
{
  [TypeLibType(4240)]
  [Guid("96B3B9C9-47A2-4214-A781-CB4F519CF86F")]
  [ComImport]
  public interface IOneSignAuthPrivate : IOneSignAuth8
  {
    [DispId(10)]
    new IntPtr ParentWindow { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(20)]
    new object User { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.IUnknown), In] set; }

    [DispId(30)]
    new bool SameUserOnly { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(40)]
    new object CurrentOneSignUser { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; }

    [DispId(50)]
    new object CurrentWindowsUser { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; }

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool Authenticate();

    [DispId(70)]
    new int AuthError { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(80)]
    new string AuthErrorMessage { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [DispId(90)]
    new string UserString { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new int GetUsedModalityCount();

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new int GetUsedModalityAt([In] int index);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetAppCredential([MarshalAs(UnmanagedType.BStr), In] string bszApp, [MarshalAs(UnmanagedType.BStr), In] string bszField);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void Reset();

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetModalityInternalName([In] int iModality);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetModalityLocalizedName([In] int iModality);

    [DispId(200)]
    new DateTime CurrentOneSignUserAuthTime { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(210)]
    new bool IsCurrentOneSignUserOnline { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(220)]
    new DateTime AuthTime { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(230)]
    new bool IsOnline { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(310)]
    new uint AuthTimeoutLimit { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(330)]
    new uint AuthFailureLimit { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(331)]
    new uint AuthFailureCount { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(340)]
    new bool AuthDomainCredsAllowed { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(350)]
    new uint AuthUserStatus { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(360)]
    new uint AuthErrorStatus { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void TestAudit();

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool PasswordAuthenticateOneSignUser(
      [MarshalAs(UnmanagedType.BStr), In] string bszUsername,
      [MarshalAs(UnmanagedType.BStr), In] string bszDomain,
      [MarshalAs(UnmanagedType.BStr), In] string bszPassword);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool KerberosAuthenticateOneSignUser([MarshalAs(UnmanagedType.BStr), In] string bszUsername, [In] IntPtr hUserToken);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void SetAppCredential([MarshalAs(UnmanagedType.BStr), In] string strApp, [MarshalAs(UnmanagedType.BStr), In] string bszField, [MarshalAs(UnmanagedType.BStr), In] string bszValue);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void LogAuditEvent([In] int nAction, [MarshalAs(UnmanagedType.BStr), In] string bszApp);

    [DispId(500)]
    new bool UseOneSignPolicySecondFactorGracePeriod { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(510)]
    new uint OverrideOneSignPolicySecondFactorGracePeriodLength { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [ComAliasName("stdole.OLE_HANDLE")]
    [DispId(600)]
    new int ParentWindow_2 { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: ComAliasName("stdole.OLE_HANDLE"), In] set; }

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void DeleteAppCredentials([MarshalAs(UnmanagedType.BStr), In] string strApp);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool OpenSession([In] uint dwSessionID);

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void CloseSession();

    [DispId(720)]
    new bool CloseSessionWithoutTerminate { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(730)]
    new uint SessionID { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(800)]
    new bool IsOneSignUserLoggedIn { [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    bool LoginAsCurrentOneSignUser();
  }
}
