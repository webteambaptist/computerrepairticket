﻿// Decompiled with JetBrains decompiler
// Type: OneSignAuthInterop.IOneSignAuth6
// Assembly: OneSignAuthInterop, Version=4.1.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2F72B1BC-2D8A-4D0A-8B25-CC84D1D2B1E2
// Assembly location: C:\Source\Web Team Legacy\ComputerRepairTicket\ComputerRepairTicket\bin\Debug\OneSignAuthInterop.dll

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace OneSignAuthInterop
{
  [TypeLibType(4288)]
  [Guid("4BAD99B6-E40F-4B78-A541-F1AB52739A6E")]
  [ComImport]
  public interface IOneSignAuth6 : IOneSignAuth5
  {
    [DispId(10)]
    new IntPtr ParentWindow { [DispId(10), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [DispId(10), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(20)]
    new object User { [DispId(20), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; [DispId(20), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.IUnknown), In] set; }

    [DispId(30)]
    new bool SameUserOnly { [DispId(30), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [DispId(30), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(40)]
    new object CurrentOneSignUser { [DispId(40), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; }

    [DispId(50)]
    new object CurrentWindowsUser { [DispId(50), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.IUnknown)] get; }

    [DispId(60)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool Authenticate();

    [DispId(70)]
    new int AuthError { [DispId(70), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(80)]
    new string AuthErrorMessage { [DispId(80), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [DispId(90)]
    new string UserString { [DispId(90), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [DispId(100)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new int GetUsedModalityCount();

    [DispId(110)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new int GetUsedModalityAt([In] int index);

    [DispId(120)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetAppCredential([MarshalAs(UnmanagedType.BStr), In] string bszApp, [MarshalAs(UnmanagedType.BStr), In] string bszField);

    [DispId(130)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void Reset();

    [DispId(140)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetModalityInternalName([In] int iModality);

    [DispId(150)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    [return: MarshalAs(UnmanagedType.BStr)]
    new string GetModalityLocalizedName([In] int iModality);

    [DispId(200)]
    new DateTime CurrentOneSignUserAuthTime { [DispId(200), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(210)]
    new bool IsCurrentOneSignUserOnline { [DispId(210), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(220)]
    new DateTime AuthTime { [DispId(220), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(230)]
    new bool IsOnline { [DispId(230), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }
        

    [DispId(310)]
    new uint AuthTimeoutLimit { [DispId(310), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(330)]
    new uint AuthFailureLimit { [DispId(330), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(331)]
    new uint AuthFailureCount { [DispId(331), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(340)]
    new bool AuthDomainCredsAllowed { [DispId(340), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(350)]
    new uint AuthUserStatus { [DispId(350), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(360)]
    new uint AuthErrorStatus { [DispId(360), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(370)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void TestAudit();

    [DispId(400)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool PasswordAuthenticateOneSignUser(
      [MarshalAs(UnmanagedType.BStr), In] string bszUsername,
      [MarshalAs(UnmanagedType.BStr), In] string bszDomain,
      [MarshalAs(UnmanagedType.BStr), In] string bszPassword);

    [DispId(410)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new bool KerberosAuthenticateOneSignUser([MarshalAs(UnmanagedType.BStr), In] string bszUsername, [In] IntPtr hUserToken);

    [DispId(420)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void SetAppCredential([MarshalAs(UnmanagedType.BStr), In] string strApp, [MarshalAs(UnmanagedType.BStr), In] string bszField, [MarshalAs(UnmanagedType.BStr), In] string bszValue);

    [DispId(430)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void LogAuditEvent([In] int nAction, [MarshalAs(UnmanagedType.BStr), In] string bszApp);

    [DispId(500)]
    new bool UseOneSignPolicySecondFactorGracePeriod { [DispId(500), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [DispId(500), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(510)]
    new uint OverrideOneSignPolicySecondFactorGracePeriodLength { [DispId(510), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [DispId(510), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(600)]
    [ComAliasName("stdole.OLE_HANDLE")]
    new int ParentWindow_2 { [DispId(600), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: ComAliasName("stdole.OLE_HANDLE"), In] set; }

    [DispId(610)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    new void DeleteAppCredentials([MarshalAs(UnmanagedType.BStr), In] string strApp);


    [DispId(700)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    bool OpenSession([In] uint dwSessionID);

    [DispId(710)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    void CloseSession();

    [DispId(720)]
    bool CloseSessionWithoutTerminate { [DispId(720), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(730)]
    uint SessionID { [DispId(730), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }
  }
}
