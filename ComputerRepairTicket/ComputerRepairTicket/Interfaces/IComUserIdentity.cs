﻿// Decompiled with JetBrains decompiler
// Type: OneSignAuthInterop.IComUserIdentity
// Assembly: OneSignAuthInterop, Version=4.1.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 2F72B1BC-2D8A-4D0A-8B25-CC84D1D2B1E2
// Assembly location: C:\Source\Web Team Legacy\ComputerRepairTicket\ComputerRepairTicket\bin\Debug\OneSignAuthInterop.dll

using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace OneSignAuthInterop
{
  [TypeLibType(4288)]
  [Guid("11660363-781B-617B-0200-128274950010")]
  [ComImport]
  public interface IComUserIdentity
  {
    [DispId(1)]
    int DirType { [DispId(1), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; [DispId(1), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: In] set; }

    [DispId(2)]
    string Username { [DispId(2), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(2), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.BStr), In] set; }

    [DispId(3)]
    string LogonTo { [DispId(3), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(3), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.BStr), In] set; }

    [DispId(5)]
    string Context { [DispId(5), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(5), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.BStr), In] set; }

    [DispId(6)]
    string DisplayName { [DispId(6), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(6), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.BStr), In] set; }

    [DispId(7)]
    string FullyQualifiedName { [DispId(7), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; }

    [DispId(8)]
    string UniqueString { [DispId(8), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [return: MarshalAs(UnmanagedType.BStr)] get; [DispId(8), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] [param: MarshalAs(UnmanagedType.BStr), In] set; }

    [DispId(9)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    void FromStrings([MarshalAs(UnmanagedType.BStr), In] string bszUsername, [MarshalAs(UnmanagedType.BStr), In] string bszLogonTo, [MarshalAs(UnmanagedType.BStr), In] string bszContext);

    [DispId(10)]
    bool IsEmpty { [DispId(10), MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)] get; }

    [DispId(11)]
    [MethodImpl(MethodImplOptions.InternalCall, MethodCodeType = MethodCodeType.Runtime)]
    bool Equals([MarshalAs(UnmanagedType.Interface), In] IComUserIdentity pUser);
  }
}
