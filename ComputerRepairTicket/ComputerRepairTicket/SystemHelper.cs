﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;
using System.Diagnostics;
using System.Management;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using static ComputerRepairTicket.Domain.RegistryHelper;
using System.ServiceProcess;
using System.Windows;

namespace ComputerRepairTicket
{
    public sealed class SystemHelper
    {
        #region Properties
        const string PHash = "9F579CB7-2EB3-4D2A-BA12-69E90ACB6AAF";
        const string SaltKey = "03690DC9-C7DB-45FE-ABCC-F58F13FCACE5";
        const string VIKey = "TI$2pIjHR$1pIa14";
        #endregion

        public static string GetRegistrySetting(string key, string valueName) //TO DO REMOVE
        {
            try
            {
                return (string)Registry.GetValue(@key, valueName, string.Empty);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public static bool SetRegistrySetting(string key, string valueName, string value)
        {
            try
            {
                Registry.SetValue(@key, valueName, value);
                return true;
            }
            catch { return false; }
        }

        /// <summary>
        /// deletes printers from citrix in registry
        /// </summary>
        /// <returns></returns>
        public static int DeleteRegistrySetting()
        {
            int keycount = 0;

            try
            {
                RegistryKey rKey = Registry.CurrentUser.OpenSubKey("Software\\Citrix\\PrinterProperties", true);
                if (rKey != null)
                {
                    if (rKey.SubKeyCount > 0)
                    {
                        string[] keys = rKey.GetSubKeyNames();
                        foreach (string key in keys)
                        {
                            RegistryKey tempKey = rKey.OpenSubKey(key);
                            keycount = keycount + tempKey.SubKeyCount;
                            rKey.DeleteSubKeyTree(key);
                        }
                    }


                }

            }
            catch (Exception ee)
            {
                return -1;
            }

            return keycount;
        }

        public static bool IsVirtualMachine()
        {
            try
            {
                if (System.Environment.MachineName.ToLower().StartsWith("vclin"))
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception ex)
            {
                return false;
            }


        }

        public static string CleanUserId(string ntID)
        {
            return ntID.ToUpper().Substring(ntID.IndexOf(@"\") + 1);
        }

        public static string CleanSql(string value)
        {
            return value != null ? value.Replace("'", "''") : string.Empty;
        }

        public static string GetIpAddress()
        {
            //to do, get serial, if starts with VCLIN, then it's a $#@&* VDI

            string localIP = "?";

            // bool isRealPC = true; //put get serial logic here

            //if (isRealPC)
            //{

            try
            {
                IPHostEntry host;

                host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (IPAddress ip in host.AddressList)
                {
                    if ((ip.AddressFamily == AddressFamily.InterNetwork) && (!ip.ToString().Contains("192")))
                    {
                        localIP = ip.ToString();
                        break;
                    }
                }
            }
            catch (Exception ee)
            { }
            //}
            //else //VDI :P
            //{

            //}

            return localIP;
        }

        public static string GetClientIP(string pCName)
        {
            //HKEY_LOCAL_MACHINE\SOFTWARE\Citrix\Ica\Session for ClientAddress

            string clientIP = string.Empty;

            try
            {
                clientIP = RegistryWOW6432.GetRegKey64(RegHive.HKEY_LOCAL_MACHINE, @"Software\Citrix\Ica\Session", "ClientAddress");

                //if(string.IsNullOrEmpty(clientIP)) //if true, maybe it's a 32 bit machine?
                //{
                //    clientIP = RegistryWOW6432.GetRegKey32(RegHive.HKEY_LOCAL_MACHINE, @"Software\Citrix\Ica\Session", "ClientAddress");
                //}               
            }
            catch (Exception ex)
            {
                Truncus.LogEvents("1", "", "", 0, "Update Machine Inventory: " + ex.Message);
            }

            return clientIP;
        }

        public static string GetClientName()
        {
            //HKEY_LOCAL_MACHINE\SOFTWARE\Citrix\Ica\Session for ClientName

            string clientName = string.Empty;

            try
            {
                clientName = Environment.ExpandEnvironmentVariables("%CLIENTNAME%").ToString();

                if (String.IsNullOrWhiteSpace(clientName) || clientName.Contains("%CLIENTNAME%"))
                {
                    clientName = Dns.GetHostName();

                    if (String.IsNullOrWhiteSpace(clientName) || !clientName.Contains("%CLIENTNAME%"))
                    {
                        clientName = Environment.MachineName;
                    }
                }


            }
            catch (Exception ex)
            {
                Truncus.LogEvents("1", "", "", 0, "Update Machine Inventory: " + ex.Message);
            }
           
            return clientName;
        }

        public static bool IsServiceRunning(string service)
        {
            try
            {
                ServiceController sc = new ServiceController(service);
                if (sc.Status == ServiceControllerStatus.Running)
                    return true;
                else
                    return false;
            }
            catch { return false; }
        }

        public static bool Compare<T>(string op, T x, T y) where T : IComparable
        {
            switch (op)
            {
                case "==": return x.CompareTo(y) == 0;
                case "!=": return x.CompareTo(y) != 0;
                case ">": return x.CompareTo(y) > 0;
                case ">=": return x.CompareTo(y) >= 0;
                case "<": return x.CompareTo(y) < 0;
                case "<=": return x.CompareTo(y) <= 0;
            }
            return false;
        }

        public static string GetUrlTitle(string url)
        {
            string title = string.Empty;
            try
            {
                WebClient x = new WebClient();
                x.Credentials = CredentialCache.DefaultNetworkCredentials;
                x.Proxy = null;
                string source = x.DownloadString(url);
                title = Regex.Match(source, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>", RegexOptions.IgnoreCase).Groups["Title"].Value.Replace(Environment.NewLine, string.Empty);
                if (title.Contains("Access to this site is blocked"))
                    title = string.Empty;
            }
            catch { title = string.Empty; }

            if (string.IsNullOrEmpty(title))
            {
                title = url.Length < 70 ? url : url.Substring(0, 69);
            }

            return title;
        }

        ////public static string GetDraggedUrl(System.Windows.Forms.DragEventArgs e)
        ////{
        ////    string url = string.Empty;
        ////    object data = e.Data.GetData("UniformResourceLocator");
        ////    if (data != null)
        ////    {
        ////        MemoryStream ms = data as MemoryStream;
        ////        byte[] bytes = ms.ToArray();
        ////        Encoding encod = Encoding.ASCII;
        ////        url = encod.GetString(bytes);
        ////        url = url.Substring(0, url.IndexOf('\0'));
        ////    }
        ////    else if (e.Data.GetData(System.Windows.Forms.DataFormats.Text) != null)
        ////    {
        ////        url = e.Data.GetData(System.Windows.Forms.DataFormats.Text).ToString();
        ////    }
        ////    return url;
        ////}

        public static bool PingComputer(string sn)
        {
            try
            {
                int timeout = 3;
                System.Net.NetworkInformation.Ping pingSender = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingOptions options = new System.Net.NetworkInformation.PingOptions();
                options.DontFragment = true;
                string ipAddressOrHostName = sn;
                string data = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                byte[] buffer = System.Text.Encoding.ASCII.GetBytes(data);
                System.Net.NetworkInformation.PingReply reply = pingSender.Send(ipAddressOrHostName, timeout, buffer, options);
                return (reply.Status == System.Net.NetworkInformation.IPStatus.Success);
            }
            catch { }
            return false;
        }

        public static ArrayList GetWMIAttributes(string qo, string whereClause)
        {
            ManagementObjectSearcher searcher;
            int i = 0;
            ArrayList hd = new ArrayList();
            try
            {
                searcher = new ManagementObjectSearcher(string.Format("SELECT * FROM {0} {1}", qo, whereClause));
                foreach (ManagementObject wmi_HD in searcher.Get())
                {
                    i++;
                    PropertyDataCollection searcherProperties = wmi_HD.Properties;
                    foreach (PropertyData sp in searcherProperties)
                    {
                        hd.Add(sp.Name + " = " + sp.Value);
                    }
                }
            }
            catch (Exception ex)
            {
                // throw ex;
            }
            return hd;
        }

        public static List<EventLogEntry> GetErrorLog(int days)
        {
            List<EventLogEntry> entries = new List<System.Diagnostics.EventLogEntry>();
            EventLog log = new
            EventLog("Application", ".", "");
            DateTime MinTimeWritten = DateTime.Now.AddDays(-days);
            foreach (EventLogEntry entry in log.Entries)
            {
                if (entry.EntryType == EventLogEntryType.Error && entry.TimeWritten >= MinTimeWritten)
                    entries.Add(entry);
            }
            return entries;
        }

        public static StringBuilder GetSystemProcesses()
        {
            StringBuilder sbProcs = new StringBuilder("<Processes>");
            try
            {
                Process[] processes = Process.GetProcesses();
                foreach (Process p in processes)
                    sbProcs.AppendLine(String.Format("<Process><Name>{0}</Name><WindowTitle>{1}</WindowTitle><ThreadCt>{2}</ThreadCt><MemorySize>{3}</MemorySize><IsResponding>{4}</IsResponding></Process>", p.ProcessName, p.MainWindowTitle, p.Threads.Count.ToString(), (p.PrivateMemorySize64 / 1024).ToString("N0") + "K", p.Responding.ToString()));
            }
            catch { }
            sbProcs.Append("</Processes>");
            return sbProcs;
        }

        public static double ConvertToGB(object value)
        {
            double dbl;
            double.TryParse(value.ToString(), out dbl);
            return dbl / 1024 / 1024 / 1024;
        }

        public static double ConvertToMB(object value)
        {
            double dbl;
            double.TryParse(value.ToString(), out dbl);
            return dbl / 1024 / 1024;
        }

        public static string Encrypt(string pTxt)
        {
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(pTxt);

            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.Zeros };
            var encryptor = symmetricKey.CreateEncryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));

            byte[] cipherTextBytes;

            using (var memoryStream = new MemoryStream())
            {
                using (var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write))
                {
                    cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);
                    cryptoStream.FlushFinalBlock();
                    cipherTextBytes = memoryStream.ToArray();
                    cryptoStream.Close();
                }
                memoryStream.Close();
            }
            return Convert.ToBase64String(cipherTextBytes);
        }

        public static string Decrypt(string encTxt)
        {
            byte[] cipherTextBytes = Convert.FromBase64String(encTxt);
            byte[] keyBytes = new Rfc2898DeriveBytes(PHash, Encoding.ASCII.GetBytes(SaltKey)).GetBytes(256 / 8);
            var symmetricKey = new RijndaelManaged() { Mode = CipherMode.CBC, Padding = PaddingMode.None };

            var decryptor = symmetricKey.CreateDecryptor(keyBytes, Encoding.ASCII.GetBytes(VIKey));
            var memoryStream = new MemoryStream(cipherTextBytes);
            var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Read);
            byte[] plainTextBytes = new byte[cipherTextBytes.Length];

            int decryptedByteCount = cryptoStream.Read(plainTextBytes, 0, plainTextBytes.Length);
            memoryStream.Close();
            cryptoStream.Close();
            return Encoding.UTF8.GetString(plainTextBytes, 0, decryptedByteCount).TrimEnd("\0".ToCharArray());
        }


    }
}