﻿using ComputerRepairTicket.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ComputerRepairTicket
{
    public sealed class Utility
    {

        static List<CacheRefresh> cList = CacheRefresh.selCacheRefresh();

        /// <summary>
        /// Generic method to deserialize cached app data
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static T Deserialize<T>(string fileName)
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                T serializedData;
                using (Stream stream = File.Open(fileName, FileMode.Open, FileAccess.Read))
                {
                    serializedData = (T)serializer.Deserialize(stream);
                }
                return serializedData;
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }

            return default(T);
        }

        /// <summary>
        /// Generic method to serialize data for caching, replaces XML write method
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dataToSerialize"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static void Serialize<T>(T dataToSerialize, string fileName)
        {
            try
            {
                using (Stream stream = File.Open(fileName, FileMode.Create, FileAccess.ReadWrite))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    XmlTextWriter writer = new XmlTextWriter(stream, Encoding.Default);
                    writer.Formatting = Formatting.Indented;
                    serializer.Serialize(writer, dataToSerialize);
                    writer.Close();
                }
            }
            catch (Exception ex)
            {
                Truncus.LogErrorEvents(ex.Message, false, Assembly.GetCallingAssembly().FullName);
            }
        }

        public static Dictionary<string, string> getSettingsfromDB()
        {
            Dictionary<string, string> settings = new Dictionary<string, string>();
            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selSettings", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    SqlDataReader sqlDataReader = sqlcmd.ExecuteReader();
                    while (sqlDataReader.Read())
                    {
                        settings.Add(sqlDataReader[0].ToString(), sqlDataReader[1].ToString());
                    }
                    sqlDataReader.Close();

                }//end try

                catch (SqlException se)
                {

                }//end catch
                catch (Exception ex)
                {

                }//end catch

            }//end sql

            return settings;
        }

        /// <summary>
        /// get value from settings cache
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns> //moved to shared
        public static string getSettingFromCache(string key)
        {
            string value = "";
            string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            try
            {
                string xmlFileName = Utility.getPath() + "CRTicket_Settings.xml";
                XDocument xmlDoc = XDocument.Load(xmlFileName);
                IEnumerable<string> result = from s in xmlDoc.Descendants("Setting")
                                             where s.Element("KeyName").Value == key
                                             select s.Element("ValueName").Value.ToString();

                value = result.FirstOrDefault().ToString();
            }
            catch (Exception ex)
            {
                string emsg = ex.Message;

            }

            return value;
        }

        /// <summary>
        /// Write the settings XML
        /// </summary>
        public static void writeSettingsCache()
        {
            string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            try
            {
                string xmlFileName = Utility.getPath() + "CRTicket_Settings.xml";
                Dictionary<string, string> settings = Utility.getSettingsfromDB();

                if (settings.Count > 0)
                {
                    try
                    {
                        if (File.Exists(xmlFileName))
                        {
                            File.Delete(xmlFileName);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    foreach (KeyValuePair<string, string> set in settings)
                    {
                        if (File.Exists(xmlFileName))
                        {
                            XDocument xmlDoc = XDocument.Load(xmlFileName);
                            xmlDoc.Element("Settings").Add(new XElement("Setting", new XElement("KeyName", set.Key), new XElement("ValueName", set.Value)));
                            xmlDoc.Save(xmlFileName);
                        }
                        else
                        {
                            //generate XML document first and save first record

                            XDocument xmlDoc = new XDocument(
                                new XDeclaration("1.0", "utf-16", "true"),
                                new XComment("Settings List for CRTicket"),
                                new XElement("Settings",
                                        new XElement("Setting",
                                            new XElement("KeyName", set.Key),
                                            new XElement("ValueName", set.Value))
                                        )
                                    );
                            xmlDoc.Save(xmlFileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        public static double retHour() //set cache freshness interval, get settings from settings, default is 1 - 3
        {
            double cacheDouble = -3;

            try
            {
                string cacheInterval = getSettingFromCache("CacheInterval");
                //return Convert.ToDouble(((new Random().Next(25, 36)) * -1));
                cacheDouble = (Convert.ToDouble(cacheInterval) * -1);
                return cacheDouble;
            }
            catch (Exception ex)
            {
                return Convert.ToDouble(((new Random().Next(3, 6)) * -1));
            }

        }

        public static double refreshSettingsInterval() //get setting from settings, default in one hour
        {
            double settingsDouble = -2;

            try
            {
                string settingsInterval = getSettingFromCache("RefreshInterval");
                //return Convert.ToDouble(((new Random().Next(25, 36)) * -1));
                settingsDouble = (Convert.ToDouble(settingsInterval) * -1);
                return settingsDouble;
            }
            catch (Exception ex)
            {
                return Convert.ToDouble(((new Random().Next(2, 4)) * -1));
            }
        }

        public static string getEmail()
        {
            string email = "";
            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlConn.Open();
                    SqlCommand sqlcmd = new SqlCommand("dbo.selDataValue", sqlConn);  //to do change to use SQL call and cache
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.Parameters.AddWithValue("datakey", "HelpDeskForm");
                    object obj = sqlcmd.ExecuteScalar();
                    email = obj.ToString();
                }//end try

                catch (SqlException se)
                { }//end catch
                catch (Exception ex)
                { }//end catch

            }//end sql

            return email;
        }

        /// <summary>
        /// Generic method to check for cache and cache freshness
        /// </summary>
        /// <param name="xmlFileName"></param>
        /// <returns></returns>
        public static bool checkCache(string xmlFileName, string cacheName)
        {
            bool isOld = true;
            try
            {
                string output = (from x in cList where x.CacheName.ToUpper() == cacheName.Trim().ToUpper() select x.RefreshInHours).First();
                double time = double.Parse(output);

                if (File.Exists(xmlFileName))
                {
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(time * -1)) //if the file is older than 24 hours
                    {
                        File.Delete(xmlFileName);
                        isOld = true;
                    }
                    else
                    {
                        isOld = false; //if the file exists and is not stale, use it
                    }
                }
            }
            catch (IOException ie)
            {
                isOld = true; //return true to delete and refresh
            }

            return isOld;

        }

        /// <summary>
        /// Generic method to check for cache and cache freshness
        /// </summary>
        /// <param name="xmlFileName"></param>
        /// <returns></returns>
        public static bool checkCache(string xmlFileName)
        {
            bool isOld = true;
            try
            {

                if (File.Exists(xmlFileName))
                {
                    if (File.GetLastWriteTime(xmlFileName) < DateTime.Now.AddHours(Utility.retHour())) //if the file is older than 24 hours
                    {
                        File.Delete(xmlFileName);
                        isOld = true;
                    }
                    else
                    {
                        isOld = false; //if the file exists and is not stale, use it
                    }
                }
            }
            catch (IOException ie)
            {
                isOld = true; //return true to delete and refresh
            }

            return isOld;

        }

        public static double DBParse(string input)
        {
            try
            {
                return double.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        public static bool BoolParse(string input)
        {
            try
            {
                return bool.Parse(input);
            }
            catch
            {
                return false;
            }
        }

        public static int IntParse(string input)
        {
            try
            {
                return int.Parse(input);
            }
            catch
            {
                return 0;
            }
        }

        public static DateTime DTParse(string input)
        {
            try
            {
                return DateTime.Parse(input);
            }
            catch
            {
                return DateTime.Now;
            }
        }

        public static string ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
                throw new ArgumentNullException("securePassword");

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }

        public static SecureString ConvertToSecureString(string strValue)
        {
            SecureString secretString = new SecureString();
            foreach (char ch in strValue)
            {
                secretString.AppendChar(ch);
            }

            return secretString;
        }

        /// <summary>
        /// Gets path with build#
        /// </summary>
        /// <returns></returns>
        public static string getPath()
        {
            string build = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();

            if (!Directory.Exists(Path.GetTempPath() + build))
            {
                Directory.CreateDirectory(Path.GetTempPath() + build);
            }

            return Path.GetTempPath() + build + "\\";
        }
    }

    public class AutoClosingMessageBox
    {
        System.Threading.Timer _timeoutTimer;
        string _caption;

        AutoClosingMessageBox(string text, string caption, int timeout)
        {
            try
            {
                _caption = caption;
                _timeoutTimer = new System.Threading.Timer(OnTimerElapsed,
                    null, timeout, System.Threading.Timeout.Infinite);
                MessageBox.Show(text, caption);
            }
            catch (Exception ex) { }
        }

        public static void Show(string text, string caption, int timeout)
        {
            try
            {
                new AutoClosingMessageBox(text, caption, timeout);
            }
            catch (Exception ex) { }
        }

        void OnTimerElapsed(object state)
        {
            try
            {
                IntPtr mbWnd = FindWindow(null, _caption);
                if (mbWnd != IntPtr.Zero)
                    SendMessage(mbWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
                _timeoutTimer.Dispose();
            }
            catch (Exception ex) { }
        }

        const int WM_CLOSE = 0x0010;
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        [System.Runtime.InteropServices.DllImport("user32.dll", CharSet = System.Runtime.InteropServices.CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);
    }
}
