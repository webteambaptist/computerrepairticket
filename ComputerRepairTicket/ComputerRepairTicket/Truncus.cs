﻿using ComputerRepairTicket.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Xml.Linq;

namespace ComputerRepairTicket
{
    public sealed class Truncus
    {
        /// <summary>
        /// Helper method for logging logic
        /// </summary>
        /// <param name="logLevel"></param>
        /// <param name="app"></param>
        /// <param name="user"></param>
        /// <param name="ElapsedMilliseconds"></param>
        /// <param name="eventType"></param>
        /// 
        public static void NotifierLogEvents(string logLevel, string app, string user, long ElapsedMilliseconds, string eventType)
        {
            if (logLevel != "0")
            {
                if (logLevel == "1")
                {
                    XMLEntry xmlEntry = new XMLEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.eventType = eventType;
                    xmlEntry.ntID = user;
                    xmlEntry.machine = System.Environment.MachineName;
                    xmlEntry.app = app;
                    xmlEntry.timeElapsed = ElapsedMilliseconds.ToString();

                    writeXMLNotifierEntry(xmlEntry);

                }

                if ((logLevel == "2") || (logLevel == "3"))
                {
                    XMLEntry xmlEntry = new XMLEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.eventType = eventType;
                    xmlEntry.ntID = user;
                    xmlEntry.machine = System.Environment.MachineName;
                    xmlEntry.app = app;
                    xmlEntry.timeElapsed = ElapsedMilliseconds.ToString();
                    writeXMLNotifierEntry(xmlEntry);
                    //writeXMLEntry(xmlEntry);
                    insEventLog(eventType, user, System.Environment.MachineName, app, 0, ElapsedMilliseconds.ToString()); //to do move to async
                }

                if (logLevel == "4")
                {
                    insEventLog(eventType, user, System.Environment.MachineName, app, 0, ElapsedMilliseconds.ToString()); //to do move to async
                }
            }
        }
        public static void LogEvents(string logLevel, string app, string user, long ElapsedMilliseconds, string eventType)
        {
            if (logLevel != "0")
            {
                if (logLevel == "1")
                {
                    XMLEntry xmlEntry = new XMLEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.eventType = eventType;
                    xmlEntry.ntID = user;
                    xmlEntry.machine = System.Environment.MachineName;
                    xmlEntry.app = app;
                    xmlEntry.timeElapsed = ElapsedMilliseconds.ToString();

                    // writeXMLNotifierEntry(xmlEntry);
                    writeXMLEntry(xmlEntry);
                }

                if ((logLevel == "2") || (logLevel == "3"))
                {
                    XMLEntry xmlEntry = new XMLEntry();
                    xmlEntry.timeStamp = DateTime.Now;
                    xmlEntry.eventType = eventType;
                    xmlEntry.ntID = user;
                    xmlEntry.machine = System.Environment.MachineName;
                    xmlEntry.app = app;
                    xmlEntry.timeElapsed = ElapsedMilliseconds.ToString();
                    // writeXMLNotifierEntry(xmlEntry);
                    writeXMLEntry(xmlEntry);
                    insEventLog(eventType, user, System.Environment.MachineName, app, 0, ElapsedMilliseconds.ToString()); //to do move to async
                }

                if (logLevel == "4")
                {
                    insEventLog(eventType, user, System.Environment.MachineName, app, 0, ElapsedMilliseconds.ToString()); //to do move to async
                }
            }
        }

        /// <summary>
        /// Set default for logLevel, assume 1 for local logging
        /// </summary>
        /// <returns></returns>
        public static string setLogLevel()
        {
            string logLevel = Utility.getSettingFromCache("LogLevel");

            try
            {
                if (string.IsNullOrEmpty(logLevel))
                {
                    logLevel = "1";
                }
            }
            catch (Exception ex)
            {
                logLevel = "1";
            }
            return logLevel;
        }

        static bool FileInUse(string path)
        {
            bool writeable = false;

            try
            {
                using (FileStream fs = new FileStream(path, FileMode.OpenOrCreate))
                {
                    writeable = fs.CanWrite;
                }
            }
            catch (IOException ex)
            {
                return true;
            }

            return writeable;
        }

        /// <summary>
        /// Writes to local CRTicket log. File name is machine name 
        /// </summary>
        public static bool writeXMLEntry(XMLEntry xmlEntry)
        {
            bool isSaved = true;

            try
            {
                string logFileName = Utility.getPath() + "ComputerRepairTicket" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";

                if (File.Exists(logFileName))
                {
                    if (!FileInUse(logFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                    else
                    {
                        Thread.Sleep(5); //wait 5ms or forget it
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                }
                else
                {
                    XDocument xmlDoc = new XDocument(
                        new XDeclaration("1.0", "utf-16", "true"),
                        new XComment("Computer Repair Ticket Launch Log"),
                        new XElement("Entries",
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed))
                                    )
                            );
                    xmlDoc.Save(logFileName);
                }
            }
            catch (IOException ioe)
            {
                // EventLog_CRTicket.WriteEntry("Computer Repair Ticket Launch", "Log Write Failed" + ioe.Message, EventLogEntryType.Information);
                isSaved = false;
            }
            catch (Exception ex)
            {
                // EventLog_CRTicket.WriteEntry("Computer Repair Ticket Launch", "Log Write Failed" + ex.Message, EventLogEntryType.Information);
                isSaved = false;
            }

            return isSaved;
        }

        public static void writeXMLNotifierEntry(XMLEntry xmlEntry)
        {

            try
            {
                string logFileName = Utility.getPath() + "Notifier" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                if (File.Exists(logFileName))
                {
                    if (!FileInUse(logFileName))
                    {
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                    else
                    {
                        Thread.Sleep(5); //wait 5ms or forget it
                        XDocument xmlDoc = XDocument.Load(logFileName);
                        xmlDoc.Element("Entries").Add(
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed)
                                    )
                                );
                        xmlDoc.Save(logFileName);
                    }
                }
                else
                {
                    XDocument xmlDoc = new XDocument(
                        new XDeclaration("1.0", "utf-16", "true"),
                        new XComment("Notifier Log"),
                        new XElement("Entries",
                            new XElement("Entry",
                                    new XElement("timestamp", xmlEntry.timeStamp.ToString()),
                                    new XElement("Event", xmlEntry.eventType),
                                    new XElement("NTID", xmlEntry.ntID),
                                    new XElement("Machine", xmlEntry.machine),
                                    new XElement("App", xmlEntry.app),
                                    new XElement("TimeLength", xmlEntry.timeElapsed))
                                    )
                            );
                    xmlDoc.Save(logFileName);
                }
            }
            catch (IOException ioe)
            {
                //  EventLog_CRTicket.WriteEntry("Notifier", "Log Write Failed" + ioe.Message, EventLogEntryType.Information);              
            }
            catch (Exception ex)
            {
                //  EventLog_CRTicket.WriteEntry("Notifier", "Log Write Failed" + ex.Message, EventLogEntryType.Information);

            }

        }

        /// <summary>
        /// write event to database
        /// </summary>
        /// <param name="eventCode"></param>
        /// <param name="NTID"></param>
        /// <param name="machine"></param>
        /// <param name="application"></param>
        /// <param name="mID"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool insEventLog(string eventCode, string NTID, string machine, string application, int mID, string length)
        {
            bool success = true;
            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insEventLog";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("EventCode", eventCode);
                    sqlcmd.Parameters.AddWithValue("NtId", NTID);
                    sqlcmd.Parameters.AddWithValue("Machine", machine);
                    sqlcmd.Parameters.AddWithValue("Application", application);
                    sqlcmd.Parameters.AddWithValue("MenuItemId", mID);
                    sqlcmd.Parameters.AddWithValue("LengthOfTime", length);

                    sqlcmd.ExecuteScalar().ToString();

                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    success = false;
                }
            }//end sql

            return success;
        }

        /// <summary>
        /// write xml for errors
        /// </summary>
        public static void LogErrorEvents(string exception, bool isFatal, string app)
        {
            try
            {
                XMLErrorEntry xmlEntry = new XMLErrorEntry();
                xmlEntry.timeStamp = DateTime.Now;
                xmlEntry.exception = exception;
                xmlEntry.CRTicketVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
                xmlEntry.serial = System.Environment.MachineName;
                xmlEntry.LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper();
                xmlEntry.currentUserId = "";
                xmlEntry.isFatal = isFatal;
                xmlEntry.ICAVersion = Models.Machine.GetIcaVersion();
                xmlEntry.IEVersion = Models.Machine.GetIEVersion();
                xmlEntry.application = app;
                insErrorLog(xmlEntry);
            }
            catch (Exception ex)
            {
                EventLog.WriteEntry("ComputerRepairTicket", ex.Message);
            }
        }

        /// <summary>
        /// write event to database
        /// </summary>
        /// <param name="eventCode"></param>
        /// <param name="NTID"></param>
        /// <param name="machine"></param>
        /// <param name="application"></param>
        /// <param name="mID"></param>
        /// <param name="length"></param>
        /// <returns></returns>
        public static bool insErrorLog(XMLErrorEntry eel)
        {
            bool success = true;
            string config = SystemHelper.Decrypt(ConfigurationManager.AppSettings["PROD"]);

            SqlCommand sqlcmd = new SqlCommand();
            using (SqlConnection sqlConn = new SqlConnection(config))
            {
                try
                {
                    sqlcmd.Connection = sqlConn;
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    sqlcmd.CommandText = "dbo.insErrorLog_CRTicket";
                    sqlcmd.Connection.Open();

                    sqlcmd.Parameters.AddWithValue("Exception", eel.exception);
                    sqlcmd.Parameters.AddWithValue("CRTicketVersion", eel.CRTicketVersion);
                    sqlcmd.Parameters.AddWithValue("Serial", eel.serial);
                    sqlcmd.Parameters.AddWithValue("LSID", eel.LSID);
                    sqlcmd.Parameters.AddWithValue("CurrentUserId", eel.currentUserId);
                    sqlcmd.Parameters.AddWithValue("IsFatal", eel.isFatal);
                    sqlcmd.Parameters.AddWithValue("ICAVersion", eel.ICAVersion);
                    sqlcmd.Parameters.AddWithValue("IEVersion", eel.IEVersion);
                    sqlcmd.Parameters.AddWithValue("Application", eel.application);
                    sqlcmd.ExecuteScalar().ToString();
                    sqlcmd.Connection.Close();
                }
                catch (Exception e)
                {
                    success = false;
                }
            }//end sql

            return success;
        }
    }
}
