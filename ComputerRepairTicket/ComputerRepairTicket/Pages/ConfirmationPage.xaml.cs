﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ComputerRepairTicket
{
    /// <summary>
    /// Interaction logic for ConfirmationPage.xaml
    /// </summary>
    public partial class ConfirmationPage : Page
    {
        private string email;
        private string uname;
        private string uid;
        private string serial;

        public ConfirmationPage()
        {
            try
            {
                Imprivata Imprivata = new Imprivata();
                Imprivata.InitImprivata();
                var userIdentity = Imprivata.getUserIdentity();
                InitializeComponent();
                if (userIdentity == null)
                {
                    userIdentityNameLabel.Content = UserPrincipal.Current.DisplayName + "!";
                    uname = getName(UserPrincipal.Current.SamAccountName);
                    emailTextBox.Content = email;
                }
                else
                {
                    userIdentityNameLabel.Content = userIdentity.DisplayName + "!";
                    uname = getName(userIdentity.Username);
                    emailTextBox.Content = email;
                }


                serial = ComputerRepairTicket.SystemHelper.GetClientName();

            }
            catch
            {
                System.Windows.MessageBox.Show("Error! Please Try again.");
                System.Windows.Application.Current.Shutdown();
            }
        }

        private void confirmButton_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = gatherAdditionalInfo();

            if (isValid)
            {
                System.Windows.MessageBox.Show("Thank You for your submission! " + ticketLabel.Content);
                System.Windows.Application.Current.Shutdown();
            }
        }

        private void submitButtonEdit_Click(object sender, RoutedEventArgs e)
        {
            bool isValid = validateEntry();
            if (isValid)
            {
                isValid = gatherAdditionalInfo();
                if (isValid)
                {
                    MessageBox.Show("Thank You for your submission! " + ticketLabel.Content);
                    Application.Current.Shutdown();
                }
                else
                {
                    MessageBox.Show("Error saving Ticket. Check values and submit again.");
                }
                //MainWindow.NavigationService.Navigate(confirmationPage);
            }
            else
            {
                System.Windows.MessageBox.Show("All Fields are NOT valid! Please Fill out the form completely and try again.");
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            //Populate the Edit page of ConfirmationPage

            //Hide and Disable confirmButton
            ConfirmationPage1.confirmButton.IsEnabled = false;
            ConfirmationPage1.confirmButton.Visibility = Visibility.Hidden;

            //Show and Enable SubmitEdit Button
            ConfirmationPage1.submitButtonEdit.IsEnabled = true;
            ConfirmationPage1.submitButtonEdit.Visibility = Visibility.Visible;

            //Pass all TextBox info along to ConfirmationPage Editables
            ConfirmationPage1.alternateNameTextBoxEdit.Text = ConfirmationPage1.alternateNameTextBoxLabel.Content.ToString();
            ConfirmationPage1.alternatePhoneNumberTextBoxEdit.Text = ConfirmationPage1.alternatePhoneNumberTextBoxLabel.Content.ToString();
            ConfirmationPage1.phoneNumberTextBoxEdit.Text = ConfirmationPage1.phoneNumberTextBoxLabel.Content.ToString();
            ConfirmationPage1.campusTextBoxEdit.Text = ConfirmationPage1.campusTextBoxLabel.Content.ToString();
            ConfirmationPage1.computerSerialTextBoxEdit.Text = ConfirmationPage1.computerSerialTextBoxLabel.Content.ToString();
            ConfirmationPage1.buildingTextBoxEdit.Text = ConfirmationPage1.buildingTextBoxLabel.Content.ToString();
            ConfirmationPage1.floorTextBoxEdit.Text = ConfirmationPage1.floorTextBoxLabel.Content.ToString();
            ConfirmationPage1.unitRoomTextBoxEdit.Text = ConfirmationPage1.unitRoomTextBoxLabel.Content.ToString();


            //Hide all TextBox information labels from the ConfirmationPage
            ConfirmationPage1.alternateNameTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.alternatePhoneNumberTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.campusTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.computerSerialTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.buildingTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.floorTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.unitRoomTextBoxLabel.Visibility = Visibility.Hidden;
            ConfirmationPage1.phoneNumberTextBoxLabel.Visibility = Visibility.Hidden;

            //Show all TextBoxes from the ConfirmationPage
            ConfirmationPage1.alternateNameTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.alternatePhoneNumberTextBoxEdit.Visibility = Visibility.Visible;

            ConfirmationPage1.campusTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.computerSerialTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.buildingTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.floorTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.unitRoomTextBoxEdit.Visibility = Visibility.Visible;
            ConfirmationPage1.phoneNumberTextBoxEdit.Visibility = Visibility.Visible;


            //Pass the TextBlock info along to the ConfirmationPage Editable
            ConfirmationPage1.textBlockEdit.Text = ConfirmationPage1.textBlockLabel.Text;

            //Change TextBlock to be Editable
            ConfirmationPage1.textBlockLabel.IsReadOnly = false;

            //Hide the TextBlock from the ConfirmationPage
            ConfirmationPage1.textBlockLabel.Visibility = Visibility.Hidden;

            //Show the TextBlockEdit from the ConfirmationPage
            ConfirmationPage1.textBlockEdit.Visibility = Visibility.Visible;

            //Pass Radio Button information
            ConfirmationPage1.noRadioButton.IsChecked = ConfirmationPage1.noRadioButton.IsChecked.Value;
            ConfirmationPage1.yesRadioButton.IsChecked = ConfirmationPage1.yesRadioButton.IsChecked.Value;

            //Enable all Radio Buttons on ConfirmationPage
            ConfirmationPage1.noRadioButton.IsEnabled = true;
            ConfirmationPage1.yesRadioButton.IsEnabled = true;
        }

        private bool validateEntry()
        {
            try
            {
                bool isValid = true;


                if (!Regex.Match(emailTextBoxEdit.Content.ToString(), @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$", RegexOptions.IgnoreCase).Success)
                {
                    isValid = false;
                    validationErrorEmail.Visibility = Visibility.Visible;
                    validationErrorEmailLabel.Visibility = Visibility.Visible;
                    validationErrorEmailLocator.Visibility = Visibility.Visible;
                }
                else
                {

                    validationErrorEmail.Visibility = Visibility.Hidden;
                    validationErrorEmailLabel.Visibility = Visibility.Hidden;
                    validationErrorEmailLocator.Visibility = Visibility.Hidden;
                }

                if (yesRadioButton.IsChecked == false && noRadioButton.IsChecked == false)
                {
                    isValid = false;
                    validationErrorRadioButtonsEdit.Visibility = Visibility.Visible;
                    validationErrorRadioButtonsLabelEdit.Visibility = Visibility.Visible;
                    validationErrorRadioButtonsLocatorEdit.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorRadioButtonsEdit.Visibility = Visibility.Hidden;
                    validationErrorRadioButtonsLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorRadioButtonsLocatorEdit.Visibility = Visibility.Hidden;
                }

                if (yesRadioButton.IsChecked == true)
                {
                    validationErrorComputerSerialNumberEdit.Visibility = Visibility.Hidden;
                    validationErrorComputerSerialNumberLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorComputerSerialNumberLocatorEdit.Visibility = Visibility.Hidden;
                }

                if (computerSerialTextBoxEdit.IsEnabled == true)
                {
                    if (noRadioButton.IsChecked == true)
                    {

                        if (string.IsNullOrEmpty(computerSerialTextBoxEdit.Text))
                        {
                            isValid = false;
                            validationErrorComputerSerialNumberEdit.Visibility = Visibility.Visible;
                            validationErrorComputerSerialNumberLabelEdit.Visibility = Visibility.Visible;
                            validationErrorComputerSerialNumberLocatorEdit.Visibility = Visibility.Visible;
                        }
                        else
                        {
                            validationErrorComputerSerialNumberEdit.Visibility = Visibility.Hidden;
                            validationErrorComputerSerialNumberLabelEdit.Visibility = Visibility.Hidden;
                            validationErrorComputerSerialNumberLocatorEdit.Visibility = Visibility.Hidden;
                        }

                    }
                }
                else
                {
                    computerSerialTextBoxEdit.Text = serial;
                }

                if (string.IsNullOrEmpty(textBlockEdit.Text))
                {
                    isValid = false;
                    validationErrorTextBlockEdit.Visibility = Visibility.Visible;
                    validationErrorTextBlockLabelEdit.Visibility = Visibility.Visible;
                    validationErrorTextBlockLocatorEdit.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorTextBlockEdit.Visibility = Visibility.Hidden;
                    validationErrorTextBlockLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorTextBlockLocatorEdit.Visibility = Visibility.Hidden;
                }

                if (!Regex.Match(phoneNumberTextBoxEdit.Text, @"^[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){3}$").Success)
                {
                    // phone number was incorrect
                    isValid = false;
                    validationErrorPhoneNumberEdit.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberLabelEdit.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberLocatorEdit.Visibility = Visibility.Visible;
                    validationErrorPhoneNumberFormatLabelEdit.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorPhoneNumberEdit.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberLocatorEdit.Visibility = Visibility.Hidden;
                    validationErrorPhoneNumberFormatLabelEdit.Visibility = Visibility.Hidden;
                }

                if (string.IsNullOrEmpty(campusTextBoxEdit.Text) || string.IsNullOrEmpty(buildingTextBoxEdit.Text) || string.IsNullOrEmpty(floorTextBoxEdit.Text) || string.IsNullOrEmpty(unitRoomTextBoxEdit.Text))
                {
                    if (string.IsNullOrEmpty(campusTextBoxEdit.Text))
                    {
                        isValid = false;
                        validationErrorCampusEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLabelEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLocatorEdit.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorCampusEdit.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(floorTextBoxEdit.Text))
                    {
                        isValid = false;
                        validationErrorFloorEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLabelEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLocatorEdit.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorFloorEdit.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(unitRoomTextBoxEdit.Text))
                    {
                        isValid = false;
                        validationErrorUnitRoomEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLabelEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLocatorEdit.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorUnitRoomEdit.Visibility = Visibility.Hidden;
                    }

                    if (string.IsNullOrEmpty(buildingTextBoxEdit.Text))
                    {
                        isValid = false;
                        validationErrorBuildingEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLabelEdit.Visibility = Visibility.Visible;
                        validationErrorLocationLocatorEdit.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        validationErrorBuildingEdit.Visibility = Visibility.Hidden;
                    }
                }
                else
                {
                    validationErrorCampusEdit.Visibility = Visibility.Hidden;
                    validationErrorBuildingEdit.Visibility = Visibility.Hidden;
                    validationErrorFloorEdit.Visibility = Visibility.Hidden;
                    validationErrorUnitRoomEdit.Visibility = Visibility.Hidden;
                    validationErrorLocationLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorLocationLocatorEdit.Visibility = Visibility.Hidden;
                }

                if (string.IsNullOrEmpty(alternateNameTextBoxEdit.Text))
                {
                    isValid = false;
                    validationErrorAlternateNameEdit.Visibility = Visibility.Visible;
                    validationErrorAlternateNameLabelEdit.Visibility = Visibility.Visible;
                    validationErrorAlternateNameLocatorEdit.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorAlternateNameEdit.Visibility = Visibility.Hidden;
                    validationErrorAlternateNameLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorAlternateNameLocatorEdit.Visibility = Visibility.Hidden;
                }

                if (!Regex.Match(alternatePhoneNumberTextBoxEdit.Text, @"^[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){2}(\.|-|[ ]?)[0-9]([0-9]){3}$").Success)
                {
                    // phone number was incorrect
                    isValid = false;
                    validationErrorAlternatePhoneNumberEdit.Visibility = Visibility.Visible;
                    validationErrorAlternatePhoneNumberLabelEdit.Visibility = Visibility.Visible;
                    validationErrorAlternatePhoneNumberLocatorEdit.Visibility = Visibility.Visible;
                }
                else
                {
                    validationErrorAlternatePhoneNumberEdit.Visibility = Visibility.Hidden;
                    validationErrorAlternatePhoneNumberLabelEdit.Visibility = Visibility.Hidden;
                    validationErrorAlternatePhoneNumberLocatorEdit.Visibility = Visibility.Hidden;
                }

                return isValid;
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show("Error! Please make sure there is an online connection.");
                System.Windows.Application.Current.Shutdown();
                return false;
            }
        }
        private bool gatherData()
        {
            bool isvalid = true;

            try
            {
                if (isvalid)
                {
                    //now populate results on ConfirmationPage1

                    //Pass all TextBox info along to ConfirmationPage1
                    ConfirmationPage1.alternateNameTextBoxLabel.Content = ConfirmationPage1.alternateNameTextBoxEdit.Text;
                    ConfirmationPage1.alternatePhoneNumberTextBoxLabel.Content = ConfirmationPage1.alternatePhoneNumberTextBoxEdit.Text;
                    ConfirmationPage1.campusTextBoxLabel.Content = ConfirmationPage1.campusTextBoxEdit.Text;
                    ConfirmationPage1.computerSerialTextBoxLabel.Content = ConfirmationPage1.computerSerialTextBoxEdit.Text;
                    ConfirmationPage1.buildingTextBoxLabel.Content = ConfirmationPage1.buildingTextBoxEdit.Text;
                    ConfirmationPage1.floorTextBoxLabel.Content = ConfirmationPage1.floorTextBoxEdit.Text;
                    ConfirmationPage1.phoneNumberTextBoxLabel.Content = ConfirmationPage1.phoneNumberTextBoxEdit.Text;


                    //Hide all TextBoxes from the ConfirmationPage1
                    ConfirmationPage1.alternateNameTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.alternatePhoneNumberTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.campusTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.computerSerialTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.buildingTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.floorTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.phoneNumberTextBoxEdit.Visibility = Visibility.Hidden;
                    ConfirmationPage1.emailTextBoxEdit.Visibility = Visibility.Hidden;

                    //Show all TextBox Labels with Information
                    ConfirmationPage1.alternateNameTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.alternatePhoneNumberTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.campusTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.computerSerialTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.buildingTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.floorTextBoxLabel.Visibility = Visibility.Visible;
                    ConfirmationPage1.phoneNumberTextBoxLabel.Visibility = Visibility.Visible;


                    //Hide the TextBlock from the ConfirmationPage1
                    ConfirmationPage1.textBlockEdit.Visibility = Visibility.Hidden;

                    //Show the TextBlock Label with Information
                    ConfirmationPage1.textBlockLabel.Visibility = Visibility.Visible;

                    //Pass the TextBlock info along to ConfirmationPage1
                    ConfirmationPage1.textBlockLabel.Text = ConfirmationPage1.textBlockEdit.Text;

                    //Change TextBlock to read only
                    ConfirmationPage1.textBlockLabel.IsReadOnly = true;

                    //Disable all Radio Buttons from the ConfirmationPage1
                    ConfirmationPage1.noRadioButton.IsEnabled = false;
                    ConfirmationPage1.yesRadioButton.IsEnabled = false;

                    //Pass all Radio Button info along to ConfirmationPage1
                    ConfirmationPage1.noRadioButton.IsChecked = ConfirmationPage1.noRadioButton.IsChecked.Value;
                    ConfirmationPage1.yesRadioButton.IsChecked = ConfirmationPage1.yesRadioButton.IsChecked.Value;

                    //Hide all Buttons from the ConfirmationPage1
                    ConfirmationPage1.submitButtonEdit.Visibility = Visibility.Hidden;

                    //Show and Enable Confirm Button
                    ConfirmationPage1.confirmButton.Visibility = Visibility.Visible;
                    ConfirmationPage1.confirmButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                //// Program.LogException(ex, false);
                isvalid = false;
            }
            if (isvalid)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool gatherAdditionalInfo()
        {
            bool isvalid = true;

            try
            {

                string ram = string.Empty;
                StringBuilder sbComputer = new StringBuilder();

                try
                {
                    System.Collections.ArrayList al = ComputerRepairTicket.SystemHelper.GetWMIAttributes("Win32_ComputerSystem", string.Empty);

                    foreach (string s in al.ToArray(typeof(string)))
                    {
                        sbComputer.AppendLine(s);
                        if (s.StartsWith("TotalPhysicalMemory = "))
                        {
                            ram = s.Substring("TotalPhysicalMemory = ".Length);
                        }
                    }
                }
                catch
                {
                    isvalid = false;
                }

                StringBuilder sbOS = new StringBuilder();

                try
                {
                    System.Collections.ArrayList al2 = ComputerRepairTicket.SystemHelper.GetWMIAttributes("Win32_OperatingSystem", "where Primary='true'");
                    foreach (string s in al2.ToArray(typeof(string)))
                    {
                        sbOS.AppendLine(s);
                    }
                }
                catch
                {
                    isvalid = false;
                }

                Models.Machine machine = (new Models.Machine
                {
                    PCName = serial,
                    LSID = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToUpper(),
                    CRTicketVersion = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString(),
                    ICAVersion = Models.Machine.GetIcaVersion(),
                    IEVersion = Models.Machine.GetIEVersion(),
                    ComputerInfo = sbComputer.ToString(),
                    OSInfo = sbOS.ToString(),
                    Processes = SystemHelper.GetSystemProcesses().ToString(),
                    Memory = ram,
                    IpAddress = SystemHelper.GetIpAddress(),
                    LastLogin = DateTime.Now
                });

                if (System.Environment.MachineName.ToLower().StartsWith("vclin")) //VDI
                {
                    try
                    {
                        machine.ComputerInfo = "HostName: " + serial + ", " + machine.ComputerInfo;
                        machine.IpAddress = ComputerRepairTicket.SystemHelper.GetClientIP(machine.PCName);
                    }
                    catch (Exception ex)
                    {
                        //just use the defaults then
                    }
                }

                if (isvalid)
                {
                    string newLine = System.Environment.NewLine;
                    textBlockEdit.Text = "Campus: " + campusTextBoxEdit.Text + newLine + "Building: " + buildingTextBoxEdit.Text + newLine + "Floor: " + floorTextBoxEdit.Text + newLine + "Unit/Room: " + unitRoomTextBoxEdit.Text + newLine + newLine + "Details of Issues: " + newLine + textBlockEdit.Text;
                    phoneNumberTextBoxEdit.Text = "Contact Phone Number: " + phoneNumberTextBoxEdit.Text;
                    //UIDLabel.Content = "User ID: " + uid;
                    //LSIDLabel.Content = "UserID of computer: " + machine.LSID;
                    alternateNameTextBoxEdit.Text = "Alternate Contact: " + alternateNameTextBoxEdit.Text;
                    alternatePhoneNumberTextBoxEdit.Text = "Alternate Phone Number: " + alternatePhoneNumberTextBoxEdit.Text;

                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine(phoneNumberTextBoxEdit.Text);
                    sb.AppendLine(textBlockEdit.Text);

                    string output = "";

                    if (string.IsNullOrEmpty(computerSerialTextBoxEdit.Text)) //only add info if problem is for this machine
                    {
                        sb.AppendLine();
                        sb.AppendLine("Machine Info ");
                        sb.AppendLine();
                        sb.AppendLine("Description: " + machine.Description);
                        sb.AppendLine("Computer Repair Ticket Version: " + machine.CRTicketVersion);
                        sb.AppendLine("ICA Version: " + machine.ICAVersion);
                        sb.AppendLine("IE Version: " + machine.IEVersion);
                        sb.AppendLine("IP Address: " + machine.IpAddress);

                        computerSerialTextBoxEdit.Text = "Computer S/N: " + serial;

                        if (!string.IsNullOrEmpty(alternateNameTextBoxEdit.Text))
                        {
                            sb.AppendLine(alternateNameTextBoxEdit.Text);
                        }
                        if (!string.IsNullOrEmpty(alternatePhoneNumberTextBoxEdit.Text))
                        {
                            sb.AppendLine(alternatePhoneNumberTextBoxEdit.Text);
                        }

                        output = sb.ToString();
                    }
                    else
                    {
                        computerSerialTextBoxEdit.Text = "Computer S/N: " + computerSerialTextBoxEdit.Text;

                        if (!string.IsNullOrEmpty(alternateNameTextBoxEdit.Text))
                        {
                            output = phoneNumberTextBoxEdit.Text + newLine + "Actual Computer Serial # pertaining to issue(s): " + serial + newLine + textBlockEdit.Text + " " + newLine + alternateNameTextBoxEdit.Text + newLine + alternatePhoneNumberTextBoxEdit.Text;
                        }
                        else
                        {
                            output = phoneNumberTextBoxEdit.Text + newLine + "Actual Computer Serial # pertaining to issue(s): " + serial + newLine + textBlockEdit.Text;
                        }
                    }

                    if (!createTicket(uname, serial, phoneNumberTextBoxEdit.Text, uid, machine.LSID, output, email))
                    {
                        sendEmail(machine);
                    }

                    try
                    {
                        Models.ServiceFormData sfd = new Models.ServiceFormData();
                        sfd.Email = email;
                        sfd.UserName = uname;
                        sfd.UserID = uid;
                        sfd.PCName = computerSerialTextBoxEdit.Text;
                        sfd.LSID = machine.LSID;
                        sfd.ContactPhone = phoneNumberTextBoxEdit.Text;
                        sfd.AltName = alternateNameTextBoxEdit.Text;
                        sfd.AltPhone = alternatePhoneNumberTextBoxEdit.Text;
                        sfd.Campus = campusTextBoxEdit.Text;
                        sfd.Building = buildingTextBoxEdit.Text;
                        sfd.Floor = floorTextBoxEdit.Text;
                        sfd.UnitRoom = unitRoomTextBoxEdit.Text;
                        sfd.Message = ticketLabel.Content.ToString() + newLine + textBlockEdit.Text;

                        if (System.Environment.MachineName.ToLower().StartsWith("vclin")) //VDI, add image serial
                        {
                            sfd.Message = "VDI Image Serial is: " + System.Environment.MachineName + newLine + sfd.Message;
                        }

                        Models.ServiceFormData.SaveFormData(sfd);
                    }
                    catch (Exception ee)
                    { }
                }
                else
                {
                    MessageBox.Show("An error occured, please try again later.");
                }
            }
            catch (Exception ex)
            {
                //// Program.LogException(ex, false);
                isvalid = false;
            }

            return isvalid;
        }

        private bool createTicket(string name, string serial, string phone, string userID, string LSID, string txtMsg, string email)
        {
            bool isSuccess = true;

            try
            {

                string user = ConfigurationManager.AppSettings["esmUser"].ToString();
                string pass = ConfigurationManager.AppSettings["esmPass"].ToString();
                string acct = ConfigurationManager.AppSettings["esmAccount"].ToString();
                string guid = ConfigurationManager.AppSettings["esmGUID"].ToString();

                string userout = SystemHelper.Decrypt(user);
                string passout = SystemHelper.Decrypt(pass);


                EZV.WebService ws = new EZV.WebService();
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ////SEND TO EZV
                //string newLine = System.Environment.NewLine;
                //string debugMessage = "acct = " + acct + newLine + "userout = " + userout + newLine + "GUID = " + guid + newLine + "serial = " + serial + newLine + "Phone = " + phone + newLine + "email = " + email + newLine + "name = " + name + newLine + "txtMsg = " + txtMsg;
                //System.Windows.MessageBox.Show(debugMessage);

                string ezvTask = ws.EZV_CreateRequest(acct, userout, passout, guid, "", "", "", serial, "3", "41", "", phone, "", email, name, "", "", "", "", "", "", email, name, "19", txtMsg, "", "", "", "", "");

                if (!ezvTask.ToUpper().Trim().StartsWith("I"))
                {
                    isSuccess = false;
                    ticketLabel.Content = "Ticket #: EMAIL";
                }
                else
                {
                    ticketLabel.Content = "Ticket #: " + ezvTask;
                }
            }
            catch (Exception ex)
            {

                isSuccess = false;
            }
            return isSuccess;
        }

        private void sendEmail(Models.Machine machine)
        {
            string mailto = Utility.getEmail();
            if (string.IsNullOrEmpty(mailto))
            {
                mailto = "servicedesk@bmcjax.com";
            }

            if (!string.IsNullOrEmpty(email))
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(email);
                message.To.Add(new MailAddress(mailto));
                // message.Bcc.Add(new MailAddress("servicedesk@bmcjax.com"));
                message.Subject = "Issue Submitted from Computer Repair Ticket";

                StringBuilder sb = new StringBuilder();
                sb.AppendLine("Please review the information below:");
                sb.AppendLine();
                sb.AppendLine("Name: " + uname);
                sb.AppendLine("UserID: " + uid);
                sb.AppendLine("S/N of machine: " + serial);
                sb.AppendLine("UserID of machine: " + machine.LSID);
                if (!string.IsNullOrEmpty(computerSerialTextBoxLabel.Content.ToString()))
                {
                    sb.AppendLine("Actual Computer Serial # pertaining to issue(s): " + computerSerialTextBoxLabel.Content.ToString());
                }
                sb.AppendLine();
                sb.AppendLine(phoneNumberTextBoxEdit.Text);
                sb.AppendLine();
                sb.AppendLine(alternateNameTextBoxEdit.Text);
                sb.AppendLine(alternatePhoneNumberTextBoxEdit.Text);
                sb.AppendLine();
                sb.AppendLine("Campus: " + campusTextBoxEdit.Text);
                sb.AppendLine("Building: " + buildingTextBoxEdit.Text);
                sb.AppendLine("Floor: " + floorTextBoxEdit.Text);
                sb.AppendLine("Unit/Room: " + unitRoomTextBoxEdit.Text);
                sb.AppendLine("Details of issues:");
                sb.AppendLine(textBlockEdit.Text);
                sb.AppendLine();
                sb.AppendLine("Machine Info ");
                sb.AppendLine();
                sb.AppendLine("Description: " + machine.Description);
                sb.AppendLine("Computer Repair Ticket Version: " + machine.CRTicketVersion);
                sb.AppendLine("ICA Version: " + machine.ICAVersion);
                sb.AppendLine("IE Version: " + machine.IEVersion);
                sb.AppendLine("Computer Info: " + machine.ComputerInfo);
                sb.AppendLine("OS Info: " + machine.OSInfo);
                sb.AppendLine("IP Address: " + machine.IpAddress);

                message.Body = sb.ToString();

                //try to attach most recent log
                try
                {
                    string logFileName = Utility.getPath() + "CRTicket" + DateTime.Now.ToString("yyyy-MM-dd") + ".log";
                    if (File.Exists(logFileName))
                    {
                        Attachment attach = new Attachment(logFileName);
                        message.Attachments.Add(attach);
                    }
                }
                catch (Exception ee)
                { }
                try
                {
                    SmtpClient smtpClient = new SmtpClient("smtp.bmcjax.com");
                    smtpClient.Port = 25;
                    smtpClient.Send(message);
                    Truncus.LogEvents("FORM SENT", "", uname, 0, "0");
                }//end try
                catch (Exception e)
                {
                    //ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
                    //log.Error(e.Message);
                }
            }
        }
        private string getName(string user)
        {
            try
            {
                PeopleSearch.People ps = new PeopleSearch.People();
                ps.UseDefaultCredentials = true;
                PeopleSearch.PrincipalInfo[] plist = ps.SearchPrincipals(user, 1, PeopleSearch.SPPrincipalType.User);
                if (plist.Length > 0)
                {
                    for (int x = 0; x < plist.Length; x++)
                    {
                        if (!string.IsNullOrEmpty(plist[x].Email))
                        {
                            uname = plist[x].DisplayName;
                            email = plist[x].Email;
                            uid = plist[x].AccountName;
                        }
                    }
                }
            }
            catch (Exception ee)
            {

            }

            if (string.IsNullOrWhiteSpace(email))
            {
                email = user + "@bh.local";
            }

            if(string.IsNullOrWhiteSpace(uname))
            {
                uname = UserPrincipal.Current.DisplayName;
            }

            if(string.IsNullOrWhiteSpace(uid))
            {
                uid = UserPrincipal.Current.SamAccountName;
            }

            return uname;
        }

        private void yesRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            computerSerialTextBoxEdit.IsEnabled = false;
            //computerSerialTextBoxEdit.Text = System.Environment.MachineName;
        }

        private void noRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            computerSerialTextBoxEdit.IsEnabled = true;
            //computerSerialTextBoxEdit.Text = null;
        }
    }
}
